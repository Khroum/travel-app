# TravelApp

TravelApp is a university class project

# Technology
Java - is a class-based, object-oriented programming language that is designed to have as few implementation dependencies as possible.

Spring- Spring Boot is an open source Java-based framework used to create a micro Service.

## How to run the application

$ git clone https://michalkubis97@bitbucket.org/Khroum/travel-app.git

Install Lombok plugin

Enable annotation processing

Install Maven dependencies

Open com.kubisfladro.travelapp package and run Application file

## Simple code example
@Service
public class EventServiceImpl implements EventService{
    @Autowired
    private EventRepository events;
    @Autowired
    private EventTicketRepository eventTickets;

    @Override
    public void addEvent(CreateEventDto createEventDto) {
        events.save(Event.fromDto(createEventDto));
    }

    @Override
    public void addEventTicket(CreateEventTicketDto createEventTicketDto) {
        eventTickets.save(EventTicket.fromDto(createEventTicketDto));
    }

    @Override
    public List<Event> getAllEvents() {
        return events.findAll();
    }

    @Override
    public List<EventTicket> getAllTickets() {
        return eventTickets.findAll();
    }

## Description
The main goal of this project is to create an application that allows users to book their tickets for events or hotel rooms on the same website.

## Authors
Marcin Flądro & Michał Kubis