package com.kubisfladro.travelapp.user;

import com.kubisfladro.travelapp.user.domain.dto.CreateUserDto;
import com.kubisfladro.travelapp.user.domain.exception.LoginAlreadyTakenException;
import com.kubisfladro.travelapp.user.service.UserService;
import com.kubisfladro.travelapp.user.service.UserServiceImpl;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class RegisterUserTest {
    UserService userService = new UserServiceImpl(new InMemoryUserRepository(),new BCryptPasswordEncoder());

    @Test(expected = LoginAlreadyTakenException.class)
    public void shouldThrowLoginAlreadyTakenException(){
        //given repository with one user
        CreateUserDto createUserDto = CreateUserDto.builder()
                .username("admin")
                .password("admin1")
                .email("asd@mail.com")
                .firstName("Fname")
                .lastName("Lname")
                .city("Tarnow")
                .postcode("33-100")
                .address("Adress 34/10")
                .phone("+48 333 333 333")
                .build();

        userService.createUser(createUserDto);

        //when another user tries to register with same name
        //then LoginAlreadyTakenException should be thrown
        userService.createUser(createUserDto);
    }
}
