package com.kubisfladro.travelapp.user;

import com.kubisfladro.travelapp.user.domain.dto.UpdatePasswordDto;
import com.kubisfladro.travelapp.user.domain.dto.CreateUserDto;
import com.kubisfladro.travelapp.user.domain.entity.User;
import com.kubisfladro.travelapp.user.service.UserService;
import com.kubisfladro.travelapp.user.service.UserServiceImpl;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class ChangePasswordTest {
    UserService userService = new UserServiceImpl(new InMemoryUserRepository(),new BCryptPasswordEncoder());
    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Test
    public void shouldChangePassword(){
        //given repository with one user
        CreateUserDto createUserDto = CreateUserDto.builder()
                .username("admin")
                .password("admin1")
                .email("asd@mail.com")
                .firstName("Fname")
                .lastName("Lname")
                .city("Tarnow")
                .postcode("33-100")
                .address("Adress 34/10")
                .phone("+48 333 333 333")
                .build();
        userService.createUser(createUserDto);

        //when user changes password
        String newPassword = "new";
        userService.changePassword(UpdatePasswordDto
                .builder()
                .userId(1L)
                .password(newPassword)
                .build());

        //then new password should be encrypted and replace the old one
        String passwordAfter = userService
                .getAllUsers()
                .stream()
                .filter(user -> user.getUsername().equals("admin"))
                .findFirst()
                .map(User::getPassword)
                .get();

        Assertions.assertTrue(passwordEncoder.matches(newPassword,passwordAfter));
    }
}
