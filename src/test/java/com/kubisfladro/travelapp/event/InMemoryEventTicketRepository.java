package com.kubisfladro.travelapp.event;

import com.kubisfladro.travelapp.event.domain.entity.Event;
import com.kubisfladro.travelapp.event.domain.entity.EventTicket;
import com.kubisfladro.travelapp.event.domain.repository.EventTicketRepository;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@FieldDefaults(level = AccessLevel.PRIVATE)
public class InMemoryEventTicketRepository implements EventTicketRepository {
    List<EventTicket> eventTickets;
    Long counter;

    public InMemoryEventTicketRepository() {
        this.eventTickets = new LinkedList<>();
        counter = 1L;
    }

    @Override
    public List<EventTicket> findAll() {
        return null;
    }

    @Override
    public List<EventTicket> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<EventTicket> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<EventTicket> findAllById(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(EventTicket entity) {

    }

    @Override
    public void deleteAll(Iterable<? extends EventTicket> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends EventTicket> S save(S entity) {
        entity.setId(counter);
        counter++;
        eventTickets.add(entity);
        return null;
    }

    @Override
    public <S extends EventTicket> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<EventTicket> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends EventTicket> S saveAndFlush(S entity) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<EventTicket> entities) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public EventTicket getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends EventTicket> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends EventTicket> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends EventTicket> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends EventTicket> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends EventTicket> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends EventTicket> boolean exists(Example<S> example) {
        return false;
    }


    @Override
    public List<EventTicket> findEventTicketsByOrderId(Long orderId) {
        return null;
    }

    @Override
    public List<EventTicket> findEventTicketsByEventId(Long eventId) {
        return eventTickets
                .stream()
                .filter(eventTicket -> eventTicket.getEventId().equals(eventId))
                .collect(Collectors.toList());
    }


}
