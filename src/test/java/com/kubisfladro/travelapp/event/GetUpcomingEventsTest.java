package com.kubisfladro.travelapp.event;

import com.kubisfladro.travelapp.event.domain.dto.CreateEventDto;
import com.kubisfladro.travelapp.event.domain.entity.Event;
import com.kubisfladro.travelapp.event.domain.enumerate.EventType;
import com.kubisfladro.travelapp.event.service.EventService;
import com.kubisfladro.travelapp.event.service.EventServiceImpl;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.time.LocalDateTime;
import java.util.List;

@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class GetUpcomingEventsTest {
    EventService eventService ;

    public GetUpcomingEventsTest() {
        eventService = new EventServiceImpl(new InMemoryEventRepository(),new InMemoryEventTicketRepository());
        setup();
    }


    void setup(){
        //given repository with 2  events that already took place and 2 events to
        CreateEventDto createEventDto1 = CreateEventDto
                .builder()
                .type(EventType.ART)
                .city("Krakow")
                .address("Krakowska 12/12")
                .name("Wystawa")
                .description("opis opis opis")
                .start(LocalDateTime.of(2015,12,29,10,0))
                .end(LocalDateTime.of(2015,12,30,10,0))
                .ticketCost(10.0)
                .imageUrl("url")
                .build();

        CreateEventDto createEventDto2 = CreateEventDto
                .builder()
                .type(EventType.MUSIC)
                .city("Tarnow")
                .address("Walowa")
                .name("Koncert")
                .description("opis opis opis")
                .start(LocalDateTime.of(2015,12,29,10,0))
                .end(LocalDateTime.of(2015,12,30,10,0))
                .ticketCost(10.0)
                .imageUrl("url")
                .build();

        CreateEventDto createEventDto3 = CreateEventDto
                .builder()
                .type(EventType.ART)
                .city("Krakow")
                .address("Dunin-Wasowicza")
                .name("Wystawa")
                .description("opis opis opis")
                .start(LocalDateTime.now().plusDays(1L))
                .end(LocalDateTime.now().plusDays(2L))
                .ticketCost(10.0)
                .imageUrl("url")
                .build();

        CreateEventDto createEventDto4 = CreateEventDto
                .builder()
                .type(EventType.MUSIC)
                .city("Tarnow")
                .address("Walowa")
                .name("Wystawa")
                .description("opis opis opis")
                .start(LocalDateTime.now().plusDays(1L))
                .end(LocalDateTime.now().plusDays(2L))
                .ticketCost(10.0)
                .imageUrl("url")
                .build();

        eventService.createEvent(createEventDto1);
        eventService.createEvent(createEventDto2);
        eventService.createEvent(createEventDto3);
        eventService.createEvent(createEventDto4);
    }

    @Test
    public void shouldGetUpcomingEvents(){
        //when user wants to get upcoming events
        List<Event> upcomingEvents = eventService.getUpcomingEvents();

        //user should get two events with start date after LocalDateTime.now()
        Assertions.assertEquals(2,upcomingEvents.size());
        upcomingEvents.forEach(event -> Assertions.assertTrue(event.getStart().isAfter(LocalDateTime.now())));
    }

    @Test
    public void shouldGetUpcomingEventsInTarnow(){
        String city = "Tarnow";

        //when user wants to get upcoming events by city Tarnow
        List<Event> upcomingEvents = eventService.getUpcomingEventsByCity(city);

        //user should get one event which will take place in Tarnow with start date after LocalDateTime.now()
        Assertions.assertEquals(1,upcomingEvents.size());
        upcomingEvents.forEach(event -> {
            Assertions.assertTrue(event.getStart().isAfter(LocalDateTime.now()));
            Assertions.assertEquals(city,event.getCity());
        });
    }

    @Test
    public void shouldGetUpcomingArtEvents(){
        EventType eventType = EventType.ART;

        //when user wants to get upcoming events by type EventType.ART
        List<Event> upcomingEvents = eventService.getUpcomingEventsByType(eventType);

        //user should get one event of type EventType.ART with start date after LocalDateTime.now()
        Assertions.assertEquals(1,upcomingEvents.size());
        upcomingEvents.forEach(event -> {
            Assertions.assertTrue(event.getStart().isAfter(LocalDateTime.now()));
            Assertions.assertEquals(eventType,event.getType());
        });
    }
}
