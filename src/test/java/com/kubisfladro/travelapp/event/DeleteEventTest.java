package com.kubisfladro.travelapp.event;

import com.kubisfladro.travelapp.event.domain.dto.CreateEventDto;
import com.kubisfladro.travelapp.event.domain.dto.CreateEventTicketDto;
import com.kubisfladro.travelapp.event.domain.enumerate.EventType;
import com.kubisfladro.travelapp.event.domain.exception.EventTicketReservationsExistException;
import com.kubisfladro.travelapp.event.service.EventService;
import com.kubisfladro.travelapp.event.service.EventServiceImpl;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.Test;

import java.time.LocalDateTime;

@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class DeleteEventTest {
    EventService eventService = new EventServiceImpl(new InMemoryEventRepository(),new InMemoryEventTicketRepository());

    @Test(expected = EventTicketReservationsExistException.class)
    public void shouldThrowEvenTicketReservationsExistException(){
        //given repository with 1 event and 1 event ticket reservation related to that event
        CreateEventDto createEventDto = CreateEventDto
                .builder()
                .type(EventType.ART)
                .city("Krakow")
                .address("Krakowska 12/12")
                .name("Wystawa")
                .description("opis opis opis")
                .start(LocalDateTime.now())
                .end(LocalDateTime.now().plusDays(1L))
                .ticketCost(10.0)
                .imageUrl("url")
                .build();

        CreateEventTicketDto createEventTicketDto = CreateEventTicketDto
                .builder()
                .eventId(1L)
                .orderId(1L)
                .quantity(4)
                .totalTicketCost(40.0)
                .build();

        eventService.createEvent(createEventDto);
        eventService.createEventTicket(createEventTicketDto);

        //when administrator wants to delete event which is related to event ticket reservation
        //then exception should be thrown
        eventService.deleteEvent(1L);

    }
}
