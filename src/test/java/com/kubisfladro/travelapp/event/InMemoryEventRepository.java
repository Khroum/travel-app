package com.kubisfladro.travelapp.event;

import com.kubisfladro.travelapp.event.domain.entity.Event;
import com.kubisfladro.travelapp.event.domain.enumerate.EventType;
import com.kubisfladro.travelapp.event.domain.repository.EventRepository;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class InMemoryEventRepository implements EventRepository {
    List<Event> events;
    Long counter;

    public InMemoryEventRepository() {
        this.events = new LinkedList<>();
        this.counter = 1L;
    }

    @Override
    public List<Event> findAll() {
        return List.copyOf(events);
    }

    @Override
    public List<Event> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Event> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<Event> findAllById(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return events.size();
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Event entity) {

    }

    @Override
    public void deleteAll(Iterable<? extends Event> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Event> S save(S entity) {
        entity.setId(counter);
        counter++;
        events.add(entity);
        return null;
    }

    @Override
    public <S extends Event> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<Event> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Event> S saveAndFlush(S entity) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Event> entities) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Event getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends Event> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Event> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Event> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Event> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Event> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Event> boolean exists(Example<S> example) {
        return false;
    }


    @Override
    public List<Event> getUpcomingEvents() {
        return events
                .stream()
                .filter(event -> event.getStart().isAfter(ChronoLocalDateTime.from(LocalDateTime.now())) )
                .collect(Collectors.toList());
    }

    @Override
    public List<Event> getUpcomingEventsByType(EventType eventType) {
        return events
                .stream()
                .filter(event -> event.getStart().isAfter(LocalDateTime.now()))
                .filter(event -> event.getType().equals(eventType))
                .collect(Collectors.toList());
    }

    @Override
    public List<Event> getUpcomingEventsByCity(String city) {
        return events
                .stream()
                .filter(event -> event.getStart().isAfter(LocalDateTime.now()))
                .filter(event -> event.getCity().equals(city))
                .collect(Collectors.toList());
    }
}
