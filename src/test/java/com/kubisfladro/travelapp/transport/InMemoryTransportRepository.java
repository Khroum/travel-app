package com.kubisfladro.travelapp.transport;

import com.kubisfladro.travelapp.transport.domain.entity.Transport;
import com.kubisfladro.travelapp.transport.domain.repository.TransportRepository;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class InMemoryTransportRepository implements TransportRepository {
    List<Transport> transports;
    Long counter;

    public InMemoryTransportRepository() {
        this.transports = new LinkedList<>();
        this.counter = 1L;
    }

    @Override
    public List<Transport> findTransportsByFromDestinationAndToDestination(String from, String to) {
        return transports
                .stream()
                .filter(transport -> transport.getFromDestination().equals(from))
                .filter(transport -> transport.getToDestination().equals(to))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteTransportsByContractor(String contractor) {

    }

    @Override
    public List<Transport> findAll() {
        return List.copyOf(transports);
    }

    @Override
    public List<Transport> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Transport> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<Transport> findAllById(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return transports.size();
    }

    @Override
    public void deleteById(Long aLong) {
        transports
                .stream()
                .filter(transport -> transport.getId().equals(aLong))
                .findFirst()
                .ifPresent(transport -> transports.remove(transport));
    }

    @Override
    public void delete(Transport entity) {
        transports.remove(entity);
    }

    @Override
    public void deleteAll(Iterable<? extends Transport> entities) {

    }

    @Override
    public void deleteAll() {
        transports.clear();
    }

    @Override
    public <S extends Transport> S save(S entity) {
        entity.setId(counter);
        transports.add(entity);
        counter++;
        return null;
    }

    @Override
    public <S extends Transport> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<Transport> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Transport> S saveAndFlush(S entity) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Transport> entities) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Transport getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends Transport> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Transport> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Transport> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Transport> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Transport> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Transport> boolean exists(Example<S> example) {
        return false;
    }
}
