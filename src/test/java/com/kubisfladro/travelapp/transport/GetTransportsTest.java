package com.kubisfladro.travelapp.transport;

import com.kubisfladro.travelapp.transport.domain.dto.CreateTransportDto;
import com.kubisfladro.travelapp.transport.domain.dto.DestinationDto;
import com.kubisfladro.travelapp.transport.domain.entity.Transport;
import com.kubisfladro.travelapp.transport.domain.enumerate.TransportClass;
import com.kubisfladro.travelapp.transport.domain.enumerate.TransportType;
import com.kubisfladro.travelapp.transport.service.TransportService;
import com.kubisfladro.travelapp.transport.service.TransportServiceImpl;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.List;

@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class GetTransportsTest {
    TransportService transportService;

    public GetTransportsTest() {
        transportService = new TransportServiceImpl(new InMemoryTransportRepository());
        setup();
    }

    void setup(){
        //given repository with 4 transports, 2 from Krakow to Tarnow
        CreateTransportDto createTransportDto1 = CreateTransportDto
                .builder()
                .type(TransportType.AIRPLANE)
                .transportClass(TransportClass.ECONOMY)
                .contractor("Contractor 1")
                .fromDestination("Krakow")
                .toDestination("Tarnow")
                .numberOfSeats(4)
                .ticketCost(30.0)
                .websiteUrl("logo")
                .logoUrl("jakaś strona")
                .build();

        CreateTransportDto createTransportDto2 = CreateTransportDto
                .builder()
                .type(TransportType.TRAIN)
                .transportClass(TransportClass.ECONOMY)
                .contractor("Contractor 1")
                .fromDestination("Krakow")
                .toDestination("Tarnow")
                .numberOfSeats(5)
                .ticketCost(30.0)
                .websiteUrl("logo")
                .logoUrl("jakaś strona")
                .build();

        CreateTransportDto createTransportDto3 = CreateTransportDto
                .builder()
                .type(TransportType.AIRPLANE)
                .transportClass(TransportClass.ECONOMY)
                .contractor("Contractor 1")
                .fromDestination("Torun")
                .toDestination("Tarnow")
                .numberOfSeats(4)
                .ticketCost(30.0)
                .websiteUrl("logo")
                .logoUrl("jakaś strona")
                .build();

        CreateTransportDto createTransportDto4 = CreateTransportDto
                .builder()
                .type(TransportType.AIRPLANE)
                .transportClass(TransportClass.ECONOMY)
                .contractor("Contractor 1")
                .fromDestination("Krakow")
                .toDestination("Torun")
                .numberOfSeats(4)
                .ticketCost(30.0)
                .websiteUrl("logo")
                .logoUrl("jakaś strona")
                .build();

        transportService.addTransport(createTransportDto1);
        transportService.addTransport(createTransportDto2);
        transportService.addTransport(createTransportDto3);
        transportService.addTransport(createTransportDto4);
    }

    @Test
    public void shouldGetTransportsFromKrakowToTarnow(){
        //when user wants to get filtered transports
        List<Transport> transports = transportService
                .getTransportsByDestination(new DestinationDto("Krakow","Tarnow"));

        //user should get 2 transports and both should be from krakow to tarnow
        Assertions.assertEquals(2,transports.size());
        transports.stream().forEach(transport -> {
            Assertions.assertEquals("Krakow",transport.getFromDestination());
            Assertions.assertEquals("Tarnow",transport.getToDestination());
        });
    }
}
