package com.kubisfladro.travelapp.hotel;

import com.kubisfladro.travelapp.hotel.domain.entity.Hotel;
import com.kubisfladro.travelapp.hotel.domain.enumerate.Stars;
import com.kubisfladro.travelapp.hotel.domain.repository.HotelRepository;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class InMemoryHotelRepository implements HotelRepository {
    List<Hotel> hotels;
    Long counter;

    public InMemoryHotelRepository() {
        this.hotels = new LinkedList<>();
        this.counter = 1L;
    }

    @Override
    public List<Hotel> findHotelsByStars(Stars stars) {
        return hotels
                .stream()
                .filter(hotel -> hotel.getStars()==stars)
                .collect(Collectors.toList());
    }

    @Override
    public List<Hotel> findHotelsByCity(String city) {
        return hotels
                .stream()
                .filter(hotel -> hotel.getCity().equals(city))
                .collect(Collectors.toList());
    }

    @Override
    public List<Hotel> findAll() {
        return List.copyOf(hotels);
    }

    @Override
    public List<Hotel> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Hotel> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<Hotel> findAllById(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {
        hotels.stream()
                .filter(hotel -> hotel.getId().equals(aLong))
                .findFirst()
                .ifPresent(hotel -> hotels.remove(hotel));
    }

    @Override
    public void delete(Hotel entity) {
        hotels.remove(entity);
    }

    @Override
    public void deleteAll(Iterable<? extends Hotel> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Hotel> S save(S entity) {
        entity.setId(counter);
        hotels.add(entity);
        counter++;
        return null;
    }

    @Override
    public <S extends Hotel> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<Hotel> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Hotel> S saveAndFlush(S entity) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Hotel> entities) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Hotel getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends Hotel> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Hotel> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Hotel> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Hotel> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Hotel> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Hotel> boolean exists(Example<S> example) {
        return false;
    }
}
