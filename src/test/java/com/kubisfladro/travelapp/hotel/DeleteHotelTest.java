package com.kubisfladro.travelapp.hotel;

import com.kubisfladro.travelapp.hotel.domain.dto.CreateHotelDto;
import com.kubisfladro.travelapp.hotel.domain.dto.CreateRoomDto;
import com.kubisfladro.travelapp.hotel.domain.dto.CreateRoomReservationDto;
import com.kubisfladro.travelapp.hotel.domain.enumerate.Standard;
import com.kubisfladro.travelapp.hotel.domain.enumerate.Stars;
import com.kubisfladro.travelapp.hotel.domain.exception.RoomReservationsExistException;
import com.kubisfladro.travelapp.hotel.service.HotelService;
import com.kubisfladro.travelapp.hotel.service.HotelServiceImpl;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.Test;

import java.time.LocalDate;

@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class DeleteHotelTest {
    HotelService hotelService = new HotelServiceImpl(new InMemoryHotelRepository(),new InMemoryRoomRepository(),new InMemoryRoomReservationRepository());

    @Test(expected = RoomReservationsExistException.class)
    public void shouldThrowActiveReservationsError(){
        //given repository with 1 hotel, 1 room and 1 active reservation
        CreateHotelDto createHotelDto = CreateHotelDto
                .builder()
                .city("Tarnow")
                .name("Jakis hotel")
                .address("Address")
                .stars(Stars.ONE)
                .imageUrl("url")
                .build();

        CreateRoomDto createRoomDto = CreateRoomDto
                .builder()
                .roomNumber(1)
                .hotelId(1L)
                .standard(Standard.SUITE)
                .numberOfBeds(3)
                .dailyCost(30.0)
                .available(true)
                .imageUrl("url")
                .build();

        CreateRoomReservationDto createRoomReservationDto = CreateRoomReservationDto
                .builder()
                .hotelId(1L)
                .orderId(1L)
                .roomNumber(1)
                .from(LocalDate.now())
                .to(LocalDate.now().plusDays(10L))
                .build();

        hotelService.addHotel(createHotelDto);
        hotelService.addRoom(createRoomDto);
        hotelService.addRoomReservation(createRoomReservationDto);

        //when administrator deletes hotel with active reservations
        //then RoomReservationsExistException should be thrown
        hotelService.deleteHotelById(1L);
    }
}
