package com.kubisfladro.travelapp.hotel;

import com.kubisfladro.travelapp.hotel.domain.entity.RoomReservation;
import com.kubisfladro.travelapp.hotel.domain.repository.RoomReservationRepository;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class InMemoryRoomReservationRepository implements RoomReservationRepository {
    List<RoomReservation> reservations;
    Long counter;

    public InMemoryRoomReservationRepository() {
        reservations = new LinkedList<>();
        counter = 1L;
    }

    @Override
    public void deleteRoomReservationsByOrderId(Long id) {
        reservations.stream()
                .filter(roomReservation -> roomReservation.getOrderId().equals(id))
                .forEach(roomReservation -> reservations.remove(roomReservation));
    }

    @Override
    public List<RoomReservation> findRoomReservationsByHotelId(Long id) {
        return reservations.stream()
                .filter(roomReservation -> roomReservation.getHotelId().equals(id))
                .collect(Collectors.toList());
    }

    @Override
    public List<RoomReservation> findRoomReservationsByOrderId(Long id) {
        return null;
    }


    @Override
    public List<RoomReservation> findAll() {
        return List.copyOf(reservations);
    }

    @Override
    public List<RoomReservation> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<RoomReservation> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<RoomReservation> findAllById(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {
        reservations.stream()
                .filter(roomReservation -> roomReservation.getId().equals(aLong))
                .findFirst()
                .ifPresent(roomReservation -> reservations.remove(roomReservation));
    }

    @Override
    public void delete(RoomReservation entity) {

    }

    @Override
    public void deleteAll(Iterable<? extends RoomReservation> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends RoomReservation> S save(S entity) {
        entity.setId(counter);
        reservations.add(entity);
        counter++;
        return null;
    }

    @Override
    public <S extends RoomReservation> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<RoomReservation> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends RoomReservation> S saveAndFlush(S entity) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<RoomReservation> entities) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public RoomReservation getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends RoomReservation> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends RoomReservation> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends RoomReservation> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends RoomReservation> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends RoomReservation> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends RoomReservation> boolean exists(Example<S> example) {
        return false;
    }
}
