package com.kubisfladro.travelapp.hotel;

import com.kubisfladro.travelapp.hotel.domain.dto.CreateHotelDto;
import com.kubisfladro.travelapp.hotel.domain.entity.Hotel;
import com.kubisfladro.travelapp.hotel.domain.enumerate.Stars;
import com.kubisfladro.travelapp.hotel.service.HotelService;
import com.kubisfladro.travelapp.hotel.service.HotelServiceImpl;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.List;

@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class GetHotelsTest {
    HotelService hotelService;

    public GetHotelsTest() {
        this.hotelService = new HotelServiceImpl(new InMemoryHotelRepository(),
                new InMemoryRoomRepository(),
                new InMemoryRoomReservationRepository());
        setup();
    }

    void setup(){
        //given repository with 4 hotels: Tarnow Stars.ONE, Tarnow Stars.THREE, Krakow Stars.ONE, Krakow Stars.THREE
        CreateHotelDto createHotelDto1 = CreateHotelDto
                .builder()
                .city("Tarnow")
                .name("Hotel")
                .address("address")
                .stars(Stars.ONE)
                .imageUrl("url")
                .build();

        CreateHotelDto createHotelDto2 = CreateHotelDto
                .builder()
                .city("Tarnow")
                .name("Hotel")
                .address("address")
                .stars(Stars.THREE)
                .imageUrl("url")
                .build();

        CreateHotelDto createHotelDto3 = CreateHotelDto
                .builder()
                .city("Krakow")
                .name("Hotel")
                .address("address")
                .stars(Stars.ONE)
                .imageUrl("url")
                .build();

        CreateHotelDto createHotelDto4 = CreateHotelDto
                .builder()
                .city("Krakow")
                .name("Hotel")
                .address("address")
                .stars(Stars.THREE)
                .imageUrl("url")
                .build();

        hotelService.addHotel(createHotelDto1);
        hotelService.addHotel(createHotelDto2);
        hotelService.addHotel(createHotelDto3);
        hotelService.addHotel(createHotelDto4);
    }

    @Test
    public void shouldGetHotelsInTarnow(){
        //when user filters by city==Tarnow
        List<Hotel> hotels = hotelService.getHotelsByCity("Tarnow");

        //user should get 2 transports
        Assertions.assertEquals(2,hotels.size());
        hotels.stream().forEach(hotel -> Assertions.assertEquals("Tarnow",hotel.getCity()));
    }

    @Test
    public void shouldGetHotelsWithOneStar(){
        //when user filters by stars==Stars.ONE
        List<Hotel> hotels = hotelService.getHotelsByStars(Stars.ONE);

        //user should get 2 transports
        Assertions.assertEquals(2,hotels.size());
        hotels.stream().forEach(hotel -> Assertions.assertEquals(Stars.ONE,hotel.getStars()));
    }
}
