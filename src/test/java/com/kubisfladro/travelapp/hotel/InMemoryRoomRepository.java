package com.kubisfladro.travelapp.hotel;

import com.kubisfladro.travelapp.hotel.domain.entity.Room;
import com.kubisfladro.travelapp.hotel.domain.repository.RoomRepository;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class InMemoryRoomRepository implements RoomRepository {
    List<Room> rooms;
    Long counter;

    public InMemoryRoomRepository() {
        rooms = new LinkedList<>();
        counter = 1L;
    }

    @Override
    public List<Room> findRoomsByHotelId(Long id) {
        return null;
    }

    @Override
    public void deleteRoomsByHotelId(Long id) {
        rooms.stream()
                .filter(room -> room.getHotelId().equals(id))
                .forEach(room -> rooms.remove(room));
    }

    @Override
    public Optional<Room> findRoomByRoomNumberAndHotelId(Integer roomNumber, Long hotelId) {
        return rooms.stream()
                .filter(room -> room.getHotelId().equals(hotelId))
                .filter(room -> room.getRoomNumber().equals(roomNumber))
                .findFirst();
    }

    @Override
    public List<Room> findAll() {
        return List.copyOf(rooms);
    }

    @Override
    public List<Room> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Room> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<Room> findAllById(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {
        rooms.stream()
                .filter(room -> room.getRoomId().equals(aLong))
                .findFirst()
                .ifPresent(room -> rooms.remove(room));
    }

    @Override
    public void delete(Room entity) {

    }

    @Override
    public void deleteAll(Iterable<? extends Room> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Room> S save(S entity) {
        entity.setRoomId(counter);
        rooms.add(entity);
        counter++;
        return null;
    }

    @Override
    public <S extends Room> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<Room> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Room> S saveAndFlush(S entity) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Room> entities) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Room getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends Room> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Room> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Room> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Room> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Room> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Room> boolean exists(Example<S> example) {
        return false;
    }
}
