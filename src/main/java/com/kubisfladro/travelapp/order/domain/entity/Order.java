package com.kubisfladro.travelapp.order.domain.entity;

import com.sun.istack.NotNull;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "orders")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    Long id;

    @NotNull
    @Column(name = "user_id")
    Long userId;

    @NotNull
    @Column(name = "event_ticket")
    Boolean event;

    @NotNull
    @Column(name = "hotel_room")
    Boolean room;

    @NotNull
    @Column(name = "placed_on", columnDefinition = "TIMESTAMP WITH TIME ZONE")
    LocalDateTime placedOn;

    @NotNull
    @Column(name = "total_cost")
    Double totalCost;

}
