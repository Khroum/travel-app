package com.kubisfladro.travelapp.order.domain.repository;

import com.kubisfladro.travelapp.order.domain.entity.Order;
import org.aspectj.weaver.ast.Or;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order,Long> {
    List<Order> findOrdersByUserId(Long id);
}
