package com.kubisfladro.travelapp.order.domain.dto;

import com.kubisfladro.travelapp.event.domain.entity.EventTicket;
import com.kubisfladro.travelapp.hotel.domain.entity.RoomReservation;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderDetailsDto {
    LocalDateTime placedOn;
    Double totalCost;
    List<RoomReservation> roomReservations;
    List<EventTicket> eventTickets;
}
