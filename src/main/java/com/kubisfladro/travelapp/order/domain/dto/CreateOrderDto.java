package com.kubisfladro.travelapp.order.domain.dto;


import lombok.*;
import lombok.experimental.FieldDefaults;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateOrderDto {
    Long userId;
    Boolean event;
    Boolean room;
    LocalDateTime placedOn;
    Double totalCost;
}
