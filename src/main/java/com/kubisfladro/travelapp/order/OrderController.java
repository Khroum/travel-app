package com.kubisfladro.travelapp.order;

import com.kubisfladro.travelapp.order.domain.dto.CreateOrderDto;
import com.kubisfladro.travelapp.order.domain.dto.OrderDetailsDto;
import com.kubisfladro.travelapp.order.domain.entity.Order;
import com.kubisfladro.travelapp.order.service.OrderService;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order")
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class OrderController {
    OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @CrossOrigin
    @PostMapping("/book-order")
    Long bookOrder(@RequestBody CreateOrderDto createOrderDto){
        return orderService.createOrder(createOrderDto);
    }

    @CrossOrigin
    @DeleteMapping("/delete-order-by-id/{id}")
    void deleteOrderById(@PathVariable Long id){
        orderService.deleteOrderById(id);
    }

    @CrossOrigin
    @GetMapping("/get-order-details-by-id/{id}")
    ResponseEntity<OrderDetailsDto> getOrderDetailsById(@PathVariable Long id){
        return ResponseEntity.ok(orderService.getOrderDetailsById(id));
    }

    @CrossOrigin
    @GetMapping("/get-orders-by-user-id/{id}")
    ResponseEntity<List<Order>> getOrdersByUserId(@PathVariable Long id){
        return ResponseEntity.ok(orderService.getOrdersByUserId(id));
    }

    @CrossOrigin
    @GetMapping("/get-all-orders")
    ResponseEntity<List<Order>> getAllOrders(){
        return ResponseEntity.ok(orderService.getAllOrders());
    }

}
