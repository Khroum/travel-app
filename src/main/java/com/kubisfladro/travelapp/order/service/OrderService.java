package com.kubisfladro.travelapp.order.service;

import com.kubisfladro.travelapp.order.domain.dto.CreateOrderDto;
import com.kubisfladro.travelapp.order.domain.dto.OrderDetailsDto;
import com.kubisfladro.travelapp.order.domain.entity.Order;

import java.util.List;

public interface OrderService {
    Long createOrder(CreateOrderDto createOrderDto);
    void deleteOrderById(Long orderId);
    OrderDetailsDto getOrderDetailsById(Long orderId);
    List<Order> getOrdersByUserId(Long userId);
    List<Order> getAllOrders();
}
