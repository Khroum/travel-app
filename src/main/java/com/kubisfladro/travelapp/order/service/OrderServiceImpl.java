package com.kubisfladro.travelapp.order.service;

import com.kubisfladro.travelapp.event.domain.repository.EventTicketRepository;
import com.kubisfladro.travelapp.hotel.domain.repository.RoomReservationRepository;
import com.kubisfladro.travelapp.order.domain.dto.CreateOrderDto;
import com.kubisfladro.travelapp.order.domain.dto.OrderDetailsDto;
import com.kubisfladro.travelapp.order.domain.entity.Order;
import com.kubisfladro.travelapp.order.domain.repository.OrderRepository;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class OrderServiceImpl implements OrderService{
    OrderRepository orderRepository;
    RoomReservationRepository roomReservationRepository;
    EventTicketRepository eventTicketRepository;


    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository,
                            RoomReservationRepository roomReservationRepository,
                            EventTicketRepository eventTicketRepository){
        this.orderRepository = orderRepository;
        this.roomReservationRepository = roomReservationRepository;
        this.eventTicketRepository = eventTicketRepository;
    }

    @Override
    public Long createOrder(CreateOrderDto createOrderDto) {
        return orderRepository.save(fromCreateOrderDto(createOrderDto)).getId();
    }

    @Override
    public void deleteOrderById(Long orderId) {
        orderRepository.deleteById(orderId);
    }

    @Override
    public OrderDetailsDto getOrderDetailsById(Long orderId) {
        return orderRepository
                .findById(orderId)
                .map(this::toOrderDetailsDto)
                .get();
    }

    @Override
    public List<Order> getOrdersByUserId(Long userId) {
        return orderRepository.findOrdersByUserId(userId);
    }

    @Override
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }


    private Order fromCreateOrderDto(CreateOrderDto createOrderDto) {
        return Order
                .builder()
                .userId(createOrderDto.getUserId())
                .event(createOrderDto.getEvent())
                .room(createOrderDto.getRoom())
                .placedOn(createOrderDto.getPlacedOn())
                .totalCost(createOrderDto.getTotalCost())
                .build();
    }

    private OrderDetailsDto toOrderDetailsDto(Order order){
        return OrderDetailsDto
                .builder()
                .placedOn(order.getPlacedOn())
                .totalCost(order.getTotalCost())
                .roomReservations(roomReservationRepository.findRoomReservationsByOrderId(order.getId()))
                .eventTickets(eventTicketRepository.findEventTicketsByOrderId(order.getId()))
                .build();
    }
}
