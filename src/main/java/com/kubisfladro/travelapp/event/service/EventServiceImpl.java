package com.kubisfladro.travelapp.event.service;

import com.kubisfladro.travelapp.event.domain.dto.UpdateEventDto;
import com.kubisfladro.travelapp.event.domain.entity.Event;
import com.kubisfladro.travelapp.event.domain.enumerate.EventType;
import com.kubisfladro.travelapp.event.domain.exception.EventTicketReservationsExistException;
import com.kubisfladro.travelapp.event.domain.repository.EventRepository;
import com.kubisfladro.travelapp.event.domain.entity.EventTicket;
import com.kubisfladro.travelapp.event.domain.repository.EventTicketRepository;
import com.kubisfladro.travelapp.event.domain.dto.CreateEventDto;
import com.kubisfladro.travelapp.event.domain.dto.CreateEventTicketDto;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class EventServiceImpl implements EventService{
    EventRepository eventRepository;
    EventTicketRepository eventTicketRepository;

    @Autowired
    public EventServiceImpl(EventRepository eventRepository, EventTicketRepository eventTicketRepository) {
        this.eventRepository = eventRepository;
        this.eventTicketRepository = eventTicketRepository;
    }

    @Override
    public void createEvent(CreateEventDto createEventDto) {
        eventRepository.save(fromEventDto(createEventDto));
    }

    @Override
    public void createEventTicket(CreateEventTicketDto createEventTicketDto) {
        eventTicketRepository.save(fromEventTicketDto(createEventTicketDto));
    }

    @Override
    public void changeEventDetails(UpdateEventDto updateEventDto) {
        eventRepository.findById(updateEventDto.getEventId())
                .ifPresent(event -> {
                    event.setType(updateEventDto.getType());
                    event.setCity(updateEventDto.getCity());
                    event.setAddress(updateEventDto.getAddress());
                    event.setName(updateEventDto.getName());
                    event.setDescription(updateEventDto.getDescription());
                    event.setStart(updateEventDto.getStart());
                    event.setEnd(updateEventDto.getEnd());
                    event.setTicketCost(updateEventDto.getTicketCost());
                    event.setImageUrl(updateEventDto.getImageUrl());
                    eventRepository.save(event);
                });
    }


    @Override
    public void deleteEvent(Long eventId) {
        if(hasExistingReservations(eventId)){
            throw new EventTicketReservationsExistException();
        }
        eventRepository.deleteById(eventId);
    }

    @Override
    public List<Event> getAllEvents() {
        return eventRepository.findAll();
    }

    @Override
    public List<Event> getUpcomingEvents() {
        return eventRepository.getUpcomingEvents();
    }

    @Override
    public List<Event> getUpcomingEventsByType(EventType eventType) {
        return eventRepository.getUpcomingEventsByType(eventType);
    }

    @Override
    public List<Event> getUpcomingEventsByCity(String city) {
        return eventRepository.getUpcomingEventsByCity(city);
    }

    @Override
    public List<EventTicket> getEventTicketsByOrderId(Long orderId) {
        return eventTicketRepository.findEventTicketsByOrderId(orderId);
    }

    @Override
    public List<EventTicket> getAllEventTickets() {
        return eventTicketRepository.findAll();
    }

    @Override
    public Event getEventById(Long id) {
        return eventRepository.findById(id).get();
    }

    private boolean hasExistingReservations(Long eventId) {
        return eventTicketRepository
                .findEventTicketsByEventId(eventId)
                .size() != 0;
    }

    private static Event fromEventDto(CreateEventDto createEventDto){
        return Event
                .builder()
                .type(createEventDto.getType())
                .city(createEventDto.getCity())
                .address(createEventDto.getAddress())
                .name(createEventDto.getName())
                .description(createEventDto.getDescription())
                .start(createEventDto.getStart())
                .end(createEventDto.getEnd())
                .ticketCost(createEventDto.getTicketCost())
                .imageUrl(createEventDto.getImageUrl())
                .build();
    }

    private static EventTicket fromEventTicketDto(CreateEventTicketDto createEventTicketDto){
        return EventTicket
                .builder()
                .eventId(createEventTicketDto.getEventId())
                .orderId(createEventTicketDto.getOrderId())
                .quantity(createEventTicketDto.getQuantity())
                .totalTicketCost(createEventTicketDto.getTotalTicketCost())
                .build();
    }

}
