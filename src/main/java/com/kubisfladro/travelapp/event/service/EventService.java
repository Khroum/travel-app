package com.kubisfladro.travelapp.event.service;

import com.kubisfladro.travelapp.event.domain.dto.UpdateEventDto;
import com.kubisfladro.travelapp.event.domain.entity.Event;
import com.kubisfladro.travelapp.event.domain.entity.EventTicket;
import com.kubisfladro.travelapp.event.domain.dto.CreateEventDto;
import com.kubisfladro.travelapp.event.domain.dto.CreateEventTicketDto;
import com.kubisfladro.travelapp.event.domain.enumerate.EventType;

import java.util.List;

public interface EventService {
    void createEvent(CreateEventDto createEventDto);
    void createEventTicket(CreateEventTicketDto createEventTicketDto);
    void changeEventDetails(UpdateEventDto updateEventDto);
    void deleteEvent(Long eventId);
    List<Event> getAllEvents();
    List<Event> getUpcomingEvents();
    List<Event> getUpcomingEventsByType(EventType eventType);
    List<Event> getUpcomingEventsByCity(String city);
    List<EventTicket> getEventTicketsByOrderId(Long orderId);
    List<EventTicket> getAllEventTickets();
    Event getEventById(Long id);

}
