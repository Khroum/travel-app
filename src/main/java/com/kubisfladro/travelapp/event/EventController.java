package com.kubisfladro.travelapp.event;

import com.kubisfladro.travelapp.event.domain.dto.EventTypeDto;
import com.kubisfladro.travelapp.event.domain.dto.UpdateEventDto;
import com.kubisfladro.travelapp.event.domain.entity.Event;
import com.kubisfladro.travelapp.event.domain.entity.EventTicket;
import com.kubisfladro.travelapp.event.domain.dto.CreateEventDto;
import com.kubisfladro.travelapp.event.domain.dto.CreateEventTicketDto;
import com.kubisfladro.travelapp.event.domain.enumerate.EventType;
import com.kubisfladro.travelapp.event.service.EventService;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/event")
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class EventController {
    EventService eventService;

    @Autowired
    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @CrossOrigin
    @PostMapping("/add-event")
    public void addEvent(@RequestBody CreateEventDto createEventDto){
        eventService.createEvent(createEventDto);
    }

    @CrossOrigin
    @GetMapping("/get-all-events")
    public ResponseEntity<List<Event>> getAllEvents(){
        return ResponseEntity.ok(eventService.getAllEvents());
    }

    @CrossOrigin
    @PostMapping("/book-ticket")
    public void bookTicket(@RequestBody CreateEventTicketDto createEventTicketDto){
        eventService.createEventTicket(createEventTicketDto);
    }

    @CrossOrigin
    @GetMapping("/get-all-tickets")
    public ResponseEntity<List<EventTicket>> getAllTickets(){
        return ResponseEntity.ok(eventService.getAllEventTickets());
    }

    @CrossOrigin
    @GetMapping("/get-event-tickets-by-order-id/{id}")
    public ResponseEntity<List<EventTicket>> getEventTicketsByOrderId(@PathVariable Long id){
        return ResponseEntity.ok(eventService.getEventTicketsByOrderId(id));
    }
    
    @CrossOrigin
    @PostMapping("/change-event-details")
    public void changeEventDetails(@RequestBody UpdateEventDto updateEventDto){
        eventService.changeEventDetails(updateEventDto);
    }

    @CrossOrigin
    @GetMapping("/get-upcoming-events")
    public ResponseEntity<List<Event>> getUpcomingEvents(){
        return ResponseEntity.ok(eventService.getUpcomingEvents());
    }

    @CrossOrigin
    @GetMapping("/get-upcoming-events-by-type/{type}")
    public ResponseEntity<List<Event>> getUpcomingEventsByType(@PathVariable String type){
        return ResponseEntity.ok(eventService.getUpcomingEventsByType(EventType.valueOf(type)));
    }

    @CrossOrigin
    @GetMapping("/get-upcoming-events-by-city/{city}")
    public ResponseEntity<List<Event>> getUpcomingEventsByCity(@PathVariable String city){
        return ResponseEntity.ok(eventService.getUpcomingEventsByCity(city));
    }

    @CrossOrigin
    @GetMapping("/get-event-by-id/{id}")
    public ResponseEntity<Event> getEventById(@PathVariable Long id){
        return ResponseEntity.ok(eventService.getEventById(id));
    }
}
