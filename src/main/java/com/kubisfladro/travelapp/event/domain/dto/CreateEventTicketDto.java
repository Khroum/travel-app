package com.kubisfladro.travelapp.event.domain.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateEventTicketDto {
    Long eventId;
    Long orderId;
    Integer quantity;
    Double totalTicketCost;
}
