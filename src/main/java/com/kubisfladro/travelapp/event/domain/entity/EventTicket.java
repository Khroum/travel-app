package com.kubisfladro.travelapp.event.domain.entity;

import com.sun.istack.NotNull;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity
@Table(name = "event_tickets")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class EventTicket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "event_ticket_id")
    Long id;

    @NotNull
    @Column(name = "event_id")
    Long eventId;

    @NotNull
    @Column(name = "order_id")
    Long orderId;

    @NotNull
    @Column(name = "quantity")
    Integer quantity;

    @NotNull
    @Column(name = "total_ticket_cost")
    Double totalTicketCost;

}
