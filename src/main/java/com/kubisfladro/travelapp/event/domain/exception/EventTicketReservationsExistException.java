package com.kubisfladro.travelapp.event.domain.exception;

public class EventTicketReservationsExistException extends RuntimeException{
    public EventTicketReservationsExistException() {
    }

    public EventTicketReservationsExistException(String message) {
        super(message);
    }
}
