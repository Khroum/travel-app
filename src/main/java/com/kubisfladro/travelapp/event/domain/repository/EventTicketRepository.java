package com.kubisfladro.travelapp.event.domain.repository;

import com.kubisfladro.travelapp.event.domain.entity.EventTicket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface EventTicketRepository extends JpaRepository<EventTicket,Long> {
    @Transactional
    List<EventTicket> findEventTicketsByOrderId(Long orderId);

    @Transactional
    List<EventTicket> findEventTicketsByEventId(Long eventId);
}
