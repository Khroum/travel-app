package com.kubisfladro.travelapp.event.domain.dto;

import com.kubisfladro.travelapp.event.domain.enumerate.EventType;
import lombok.*;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EventTypeDto {
    EventType eventType;
}
