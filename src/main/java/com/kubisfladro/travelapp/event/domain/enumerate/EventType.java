package com.kubisfladro.travelapp.event.domain.enumerate;

public enum EventType {
    MUSIC,
    CULTURE,
    SPORT,
    HUMOR,
    ART,
    TECHNOLOGY,
    MOTORIZATION
}
