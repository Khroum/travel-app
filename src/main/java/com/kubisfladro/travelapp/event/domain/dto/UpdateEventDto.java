package com.kubisfladro.travelapp.event.domain.dto;

import com.kubisfladro.travelapp.event.domain.enumerate.EventType;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UpdateEventDto {
    Long eventId;
    EventType type;
    String city;
    String address;
    String name;
    String description;
    LocalDateTime start;
    LocalDateTime end;
    Double ticketCost;
    String imageUrl;
}
