package com.kubisfladro.travelapp.event.domain.repository;

import com.kubisfladro.travelapp.event.domain.entity.Event;
import com.kubisfladro.travelapp.event.domain.enumerate.EventType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event,Long> {
    @Query("SELECT e FROM Event e WHERE e.start >= CURRENT_TIMESTAMP")
    List<Event> getUpcomingEvents();

    @Query("SELECT e FROM Event e WHERE e.start >= CURRENT_TIMESTAMP and e.type = :eventType")
    List<Event> getUpcomingEventsByType(EventType eventType);

    @Query("SELECT e FROM Event e WHERE e.start >= CURRENT_TIMESTAMP and e.city = :city")
    List<Event> getUpcomingEventsByCity(String city);
}
