package com.kubisfladro.travelapp.event.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.kubisfladro.travelapp.event.domain.enumerate.EventType;
import com.sun.istack.NotNull;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "events")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "event_id")
    Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "event_type")
    EventType type;

    @NotNull
    @Column(name = "city")
    String city;

    @NotNull
    @Column(name = "address")
    String address;

    @NotNull
    @Column(name = "name")
    String name;

    @NotNull
    @Column(name = "description")
    String description;

    @NotNull
    @Column(name = "start_time", columnDefinition = "TIMESTAMP WITH TIME ZONE")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(pattern="dd/MM/yyyy hh:mm")
    LocalDateTime start;

    @NotNull
    @Column(name = "end_time", columnDefinition = "TIMESTAMP WITH TIME ZONE")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(pattern="dd/MM/yyyy hh:mm")
    LocalDateTime end;

    @NotNull
    @Column(name = "ticket_cost")
    Double ticketCost;

    @NotNull
    @Column(name = "image_url")
    String imageUrl;


}
