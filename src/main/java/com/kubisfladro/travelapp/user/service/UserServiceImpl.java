package com.kubisfladro.travelapp.user.service;

import com.kubisfladro.travelapp.user.domain.dto.*;
import com.kubisfladro.travelapp.user.domain.entity.User;
import com.kubisfladro.travelapp.user.domain.exception.LoginAlreadyTakenException;
import com.kubisfladro.travelapp.user.domain.repository.UserRepository;
import com.kubisfladro.travelapp.user.domain.enumerate.UserRole;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserServiceImpl implements UserService {
    UserRepository userRepository;
    PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public void createUser(CreateUserDto createUserDto) {
        if(isLoginTaken(createUserDto.getUsername())){
            throw new LoginAlreadyTakenException("Username "+createUserDto.getUsername()+" is already taken.");
        }
        userRepository.save(fromDto(createUserDto));
    }

    @Override
    public Long getUserIdByName(String username) {
        return userRepository
                .findUserByUsername(username)
                .map(User::getId)
                .get();
    }

    @Override
    public UserDataDto getUserData(Long userId) {
        return userRepository
                .findById(userId)
                .map(this::toUserDataDto)
                .get();
    }

    @Override
    public void changePassword(UpdatePasswordDto updatePasswordDto) {
        userRepository.findById(updatePasswordDto.getUserId())
                .ifPresent(user -> {
                    user.setPassword(passwordEncoder.encode(updatePasswordDto.getPassword()));
                    userRepository.save(user);
                });
    }

    @Override
    public void updateUserData(UpdateUserDataDto updateUserDataDto) {
        userRepository.findById(updateUserDataDto.getUserId())
                .ifPresent(user -> {
                    user.setEmail(updateUserDataDto.getEmail());
                    user.setFirstName(updateUserDataDto.getFirstName());
                    user.setLastName(updateUserDataDto.getLastName());
                    user.setCity(updateUserDataDto.getCity());
                    user.setPostcode(updateUserDataDto.getPostcode());
                    user.setAddress(updateUserDataDto.getAddress());
                    user.setPhone(updateUserDataDto.getPhone());
                    userRepository.save(user);
                });
    }

    @Override
    public void deleteUserById(Long userId) {
        userRepository.deleteById(userId);
    }

    private boolean isLoginTaken(String login) {
        return userRepository
                .findUserByUsername(login)
                .isPresent();
    }

    private UserDataDto toUserDataDto(User user) {
        return UserDataDto.
                builder()
                .email(user.getEmail())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .city(user.getCity())
                .postcode(user.getPostcode())
                .address(user.getAddress())
                .phone(user.getPhone())
                .build();
    }

    private User fromDto(CreateUserDto createUserDto){
        return User
                .builder()
                .username(createUserDto.getUsername())
                .password(passwordEncoder.encode(createUserDto.getPassword()))
                .role(UserRole.USER)
                .email(createUserDto.getEmail())
                .firstName(createUserDto.getFirstName())
                .lastName(createUserDto.getLastName())
                .city(createUserDto.getCity())
                .postcode(createUserDto.getPostcode())
                .address(createUserDto.getAddress())
                .phone(createUserDto.getPhone())
                .build();
    }
}
