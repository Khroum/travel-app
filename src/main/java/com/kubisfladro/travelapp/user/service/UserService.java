package com.kubisfladro.travelapp.user.service;

import com.kubisfladro.travelapp.user.domain.dto.*;
import com.kubisfladro.travelapp.user.domain.entity.User;

import java.util.List;

public interface UserService {
    List<User> getAllUsers();
    void createUser(CreateUserDto createUserDto);
    Long getUserIdByName(String username);
    UserDataDto getUserData(Long userId);
    void changePassword(UpdatePasswordDto updatePasswordDto);
    void updateUserData(UpdateUserDataDto updateUserDataDto);
    void deleteUserById(Long userId);
}
