package com.kubisfladro.travelapp.user;

import com.kubisfladro.travelapp.user.domain.dto.UpdatePasswordDto;
import com.kubisfladro.travelapp.user.domain.dto.UpdateUserDataDto;
import com.kubisfladro.travelapp.user.domain.dto.UserDataDto;
import com.kubisfladro.travelapp.user.domain.entity.User;
import com.kubisfladro.travelapp.user.domain.dto.CreateUserDto;
import com.kubisfladro.travelapp.user.service.UserService;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class UserController {
    UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/get-all-users")
    @CrossOrigin
    public ResponseEntity<List<User>> getAllUsers(){
        return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
    }

    @GetMapping("/get-user-data/{id}")
    @CrossOrigin
    public ResponseEntity<UserDataDto> getUserData(@PathVariable Long id){
        return new ResponseEntity<>(userService.getUserData(id),HttpStatus.OK);
    }

    @PostMapping("/change-password")
    @CrossOrigin
    public void changePassword(@RequestBody UpdatePasswordDto updatePasswordDto){
        userService.changePassword(updatePasswordDto);
    }
    @PostMapping("/change-user-data")
    @CrossOrigin
    public void changeUserData(@RequestBody UpdateUserDataDto updateUserDataDto){
        userService.updateUserData(updateUserDataDto);
    }

    @PostMapping("/register")
    @CrossOrigin
    public void createUser(@RequestBody CreateUserDto createUserDto){
        userService.createUser(createUserDto);
    }

    @DeleteMapping("delete-user/{id}")
    @CrossOrigin
    public void deleteUserAccount(@PathVariable Long id){
        userService.deleteUserById(id);
    }


}
