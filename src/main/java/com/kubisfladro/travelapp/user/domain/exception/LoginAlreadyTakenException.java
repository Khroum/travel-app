package com.kubisfladro.travelapp.user.domain.exception;

public class LoginAlreadyTakenException extends RuntimeException{
    public LoginAlreadyTakenException() {
    }

    public LoginAlreadyTakenException(String message) {
        super(message);
    }
}
