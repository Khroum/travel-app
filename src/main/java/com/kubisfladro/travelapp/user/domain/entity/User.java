package com.kubisfladro.travelapp.user.domain.entity;

import com.kubisfladro.travelapp.user.domain.enumerate.UserRole;
import com.sun.istack.NotNull;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity
@Table(name = "users")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    Long id;

    @NotNull
    @Column(name = "username", unique = true)
    String username;

    @NotNull
    @Column(name = "password")
    String password;

    @NotNull
    @Column(name = "role")
    UserRole role;

    @NotNull
    @Column(name = "email")
    String email;

    @NotNull
    @Column(name = "first_name")
    String firstName;

    @NotNull
    @Column(name = "last_name")
    String lastName;

    @NotNull
    @Column(name = "city")
    String city;

    @NotNull
    @Column(name = "postcode")
    String postcode;

    @NotNull
    @Column(name = "address")
    String address;

    @NotNull
    @Column(name = "phone")
    String phone;

}