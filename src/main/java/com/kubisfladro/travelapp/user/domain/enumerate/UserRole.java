package com.kubisfladro.travelapp.user.domain.enumerate;

public enum UserRole {
    ADMIN,
    USER
}
