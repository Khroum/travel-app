package com.kubisfladro.travelapp.user.domain.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UpdatePasswordDto {
    Long userId;
    String password;
}
