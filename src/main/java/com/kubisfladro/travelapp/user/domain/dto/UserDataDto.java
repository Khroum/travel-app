package com.kubisfladro.travelapp.user.domain.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserDataDto {
    String email;
    String firstName;
    String lastName;
    String city;
    String postcode;
    String address;
    String phone;
}
