package com.kubisfladro.travelapp.hotel;

import com.kubisfladro.travelapp.hotel.domain.dto.*;
import com.kubisfladro.travelapp.hotel.domain.entity.Hotel;
import com.kubisfladro.travelapp.hotel.domain.entity.Room;
import com.kubisfladro.travelapp.hotel.domain.entity.RoomReservation;
import com.kubisfladro.travelapp.hotel.domain.enumerate.Stars;
import com.kubisfladro.travelapp.hotel.service.HotelService;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/hotel")
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class HotelController {
    HotelService hotelService;

    @Autowired
    public HotelController(HotelService hotelService) {
        this.hotelService = hotelService;
    }

    @CrossOrigin
    @PostMapping("/add-hotel")
    void addHotel(@RequestBody CreateHotelDto createHotelDto){
        hotelService.addHotel(createHotelDto);
    }

    @CrossOrigin
    @PostMapping("/add-room")
    void addRoom(@RequestBody CreateRoomDto createRoomDto){
        hotelService.addRoom(createRoomDto);
    }

    @CrossOrigin
    @PostMapping("/book-reservation")
    void bookReservation(@RequestBody CreateRoomReservationDto createRoomReservationDto){
        hotelService.addRoomReservation(createRoomReservationDto);
    }

    @CrossOrigin
    @GetMapping("/get-all-hotels")
    ResponseEntity<List<Hotel>> getAllHotels(){
        return new ResponseEntity<>(hotelService.getAllHotels(), HttpStatus.ACCEPTED);
    }

    @CrossOrigin
    @GetMapping("/get-hotel-by-id/{id}")
    ResponseEntity<Hotel> getHotelById(@PathVariable Long id){
        return new ResponseEntity<>(hotelService.getHotelById(id),HttpStatus.ACCEPTED);
    }

    @CrossOrigin
    @GetMapping("/get-all-rooms")
    ResponseEntity<List<Room>> getAllRooms(){
        return new ResponseEntity<>(hotelService.getAllRooms(), HttpStatus.ACCEPTED);
    }

    @CrossOrigin
    @GetMapping("/get-all-reservations")
    ResponseEntity<List<RoomReservation>> getAllReservations(){
        return new ResponseEntity<>(hotelService.getAllReservations(), HttpStatus.ACCEPTED);
    }

    @CrossOrigin
    @PostMapping("/change-hotel-details")
    void changeHotelDetails(@RequestBody UpdateHotelDto updateHotelDto){
        hotelService.changeHotelDetails(updateHotelDto);
    }

    @CrossOrigin
    @PostMapping("/change-room-details")
    void changeRoomDetails(@RequestBody UpdateRoomDto updateRoomDto){
        hotelService.changeRoomDetails(updateRoomDto);
    }

    @CrossOrigin
    @DeleteMapping("/delete-hotel-by-id/{id}")
    void deleteHotelById(@PathVariable Long id){
        hotelService.deleteHotelById(id);
    }

    @CrossOrigin
    @DeleteMapping("/delete-room-by-id/{id}")
    void deleteRoomById(@PathVariable Long id){
        hotelService.deleteRoomById(id);
    }

    @CrossOrigin
    @DeleteMapping("/delete-rooms-by-hotel-id/{id}")
    void deleteRoomsByHotelIdById(@PathVariable Long id){
        hotelService.deleteRoomsByHotelId(id);
    }

    @CrossOrigin
    @DeleteMapping("/delete-room-reservations-by-order-id/{id}")
    void deleteRoomReservationsByOrderId(@PathVariable Long id){
        hotelService.deleteRoomReservationsByOrderId(id);
    }

    @CrossOrigin
    @GetMapping("/get-hotels-by-city/{city}")
    ResponseEntity<List<Hotel>> getHotelsByCity(@PathVariable String city){
        return ResponseEntity.ok(hotelService.getHotelsByCity(city));
    }

    @CrossOrigin
    @GetMapping("/get-hotels-by-stars/{stars}")
    ResponseEntity<List<Hotel>> getHotelsByStars(@PathVariable String stars){
        return ResponseEntity.ok(hotelService.getHotelsByStars(Stars.valueOf(stars)));
    }

    @CrossOrigin
    @GetMapping("/get-rooms-by-hotel-id/{id}")
    ResponseEntity<List<Room>> getRoomsByHotelId(@PathVariable Long id){
        return ResponseEntity.ok(hotelService.getRoomsByHotelId(id));
    }

}
