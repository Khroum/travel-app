package com.kubisfladro.travelapp.hotel.service;

import com.kubisfladro.travelapp.hotel.domain.dto.*;
import com.kubisfladro.travelapp.hotel.domain.entity.Hotel;
import com.kubisfladro.travelapp.hotel.domain.entity.Room;
import com.kubisfladro.travelapp.hotel.domain.entity.RoomReservation;
import com.kubisfladro.travelapp.hotel.domain.enumerate.Stars;

import java.util.List;

public interface HotelService {
    void addHotel(CreateHotelDto createHotelDto);
    void addRoom(CreateRoomDto createRoomDto);
    void addRoomReservation(CreateRoomReservationDto createRoomReservationDto);
    void changeHotelDetails(UpdateHotelDto updateHotelDto);
    void changeRoomDetails(UpdateRoomDto updateRoomDto);
    void deleteHotelById(Long hotelId);
    void deleteRoomById(Long roomId);
    void deleteRoomsByHotelId(Long hotelId);
    void deleteRoomReservationsByOrderId(Long orderId);
    List<Hotel> getHotelsByCity(String city);
    List<Hotel> getHotelsByStars(Stars stars);
    List<Room> getRoomsByHotelId(Long hotelId);
    List<Hotel> getAllHotels();
    List<Room> getAllRooms();
    List<RoomReservation> getAllReservations();
    Hotel getHotelById(Long id);
}
