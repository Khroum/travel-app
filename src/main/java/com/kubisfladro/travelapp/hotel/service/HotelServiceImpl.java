package com.kubisfladro.travelapp.hotel.service;

import com.kubisfladro.travelapp.hotel.domain.dto.*;
import com.kubisfladro.travelapp.hotel.domain.entity.Hotel;
import com.kubisfladro.travelapp.hotel.domain.entity.Room;
import com.kubisfladro.travelapp.hotel.domain.entity.RoomReservation;
import com.kubisfladro.travelapp.hotel.domain.enumerate.Stars;
import com.kubisfladro.travelapp.hotel.domain.exception.RoomReservationsExistException;
import com.kubisfladro.travelapp.hotel.domain.repository.HotelRepository;
import com.kubisfladro.travelapp.hotel.domain.repository.RoomRepository;
import com.kubisfladro.travelapp.hotel.domain.repository.RoomReservationRepository;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class HotelServiceImpl implements HotelService {
    HotelRepository hotelRepository;
    RoomRepository roomRepository;
    RoomReservationRepository reservationRepository;

    @Autowired
    public HotelServiceImpl(HotelRepository hotelRepository, RoomRepository roomRepository, RoomReservationRepository reservationRepository) {
        this.hotelRepository = hotelRepository;
        this.roomRepository = roomRepository;
        this.reservationRepository = reservationRepository;
    }

    @Override
    public void addHotel(CreateHotelDto createHotelDto) {
        hotelRepository.save(fromCreateHotelDto(createHotelDto));
    }

    @Override
    public void addRoom(CreateRoomDto createRoomDto) {
        roomRepository.save(fromCreateRoomDto(createRoomDto));
    }

    @Override
    public void addRoomReservation(CreateRoomReservationDto createRoomReservationDto) {
        roomRepository.findRoomByRoomNumberAndHotelId
                (createRoomReservationDto.getRoomNumber(), createRoomReservationDto.getHotelId())
                .ifPresent(room -> {
                    room.setAvailable(false);
                    roomRepository.save(room);
                } );
        reservationRepository.save(fromCreateRoomReservationDto(createRoomReservationDto));
    }

    @Override
    public void changeHotelDetails(UpdateHotelDto updateHotelDto) {
        hotelRepository.findById(updateHotelDto.getHotelId())
                .ifPresent(hotel -> {
                       hotel.setCity(updateHotelDto.getCity());
                       hotel.setName(updateHotelDto.getName());
                       hotel.setAddress(updateHotelDto.getAddress());
                       hotel.setStars(updateHotelDto.getStars());
                       hotel.setImageUrl(updateHotelDto.getImageUrl());
                       hotelRepository.save(hotel);
                });
    }

    @Override
    public void changeRoomDetails(UpdateRoomDto updateRoomDto) {
        roomRepository.findById(updateRoomDto.getRoomId())
                .ifPresent(room -> {
                    room.setRoomNumber(updateRoomDto.getRoomNumber());
                    room.setHotelId(updateRoomDto.getHotelId());
                    room.setStandard(updateRoomDto.getStandard());
                    room.setNumberOfBeds(updateRoomDto.getNumberOfBeds());
                    room.setDailyCost(updateRoomDto.getDailyCost());
                    room.setAvailable(updateRoomDto.getAvailable());
                    room.setImageUrl(updateRoomDto.getImageUrl());
                    roomRepository.save(room);
                });
    }

    @Override
    public void deleteHotelById(Long hotelId) {
        hasActiveReservations(hotelId);
        hotelRepository.deleteById(hotelId);
        deleteRoomsByHotelId(hotelId);
    }


    @Override
    public void deleteRoomById(Long roomId) {
        roomRepository.deleteById(roomId);
    }

    @Override
    public void deleteRoomsByHotelId(Long hotelId) {
        roomRepository.deleteRoomsByHotelId(hotelId);
    }

    @Override
    public void deleteRoomReservationsByOrderId(Long orderId) {
        reservationRepository.deleteRoomReservationsByOrderId(orderId);
    }

    @Override
    public List<Hotel> getHotelsByCity(String city) {
        return hotelRepository.findHotelsByCity(city);
    }

    @Override
    public List<Hotel> getHotelsByStars(Stars stars) {
        return hotelRepository.findHotelsByStars(stars);
    }

    @Override
    public List<Room> getRoomsByHotelId(Long hotelId) {
        return roomRepository.findRoomsByHotelId(hotelId);
    }

    @Override
    public List<Hotel> getAllHotels() {
        return hotelRepository.findAll();
    }

    @Override
    public List<Room> getAllRooms() {
        return roomRepository.findAll();
    }

    @Override
    public List<RoomReservation> getAllReservations() {
        return reservationRepository.findAll();
    }

    @Override
    public Hotel getHotelById(Long id) {
        return hotelRepository.findById(id).get();
    }

    private void hasActiveReservations(Long hotelId) {
        reservationRepository
                .findRoomReservationsByHotelId(hotelId)
                .forEach(roomReservation -> {
                    if (roomReservation.getTo().isAfter(LocalDate.now())){
                        throw new RoomReservationsExistException("This hotel has active reservations.");
                    }
                });
    }

    private static Hotel fromCreateHotelDto(CreateHotelDto createHotelDto){
        return Hotel
                .builder()
                .city(createHotelDto.getCity())
                .name(createHotelDto.getName())
                .address(createHotelDto.getAddress())
                .stars(createHotelDto.getStars())
                .imageUrl(createHotelDto.getImageUrl())
                .build();
    }

    private static Room fromCreateRoomDto(CreateRoomDto createRoomDto){
        return Room
                .builder()
                .roomNumber(createRoomDto.getRoomNumber())
                .hotelId(createRoomDto.getHotelId())
                .standard(createRoomDto.getStandard())
                .numberOfBeds(createRoomDto.getNumberOfBeds())
                .dailyCost(createRoomDto.getDailyCost())
                .available(createRoomDto.getAvailable())
                .imageUrl(createRoomDto.getImageUrl())
                .build();
    }

    private static RoomReservation fromCreateRoomReservationDto(CreateRoomReservationDto createRoomReservationDto){
        return RoomReservation
                .builder()
                .hotelId(createRoomReservationDto.getHotelId())
                .orderId(createRoomReservationDto.getOrderId())
                .roomNumber(createRoomReservationDto.getRoomNumber())
                .from(createRoomReservationDto.getFrom())
                .to(createRoomReservationDto.getTo())
                .reservationCost(createRoomReservationDto.getReservationCost())
                .build();
    }
}
