package com.kubisfladro.travelapp.hotel.domain.repository;

import com.kubisfladro.travelapp.hotel.domain.entity.RoomReservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoomReservationRepository extends JpaRepository<RoomReservation,Long> {
    void deleteRoomReservationsByOrderId(Long id);
    List<RoomReservation> findRoomReservationsByHotelId(Long id);
    List<RoomReservation> findRoomReservationsByOrderId(Long id);
}
