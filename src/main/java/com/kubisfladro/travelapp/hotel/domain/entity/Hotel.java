package com.kubisfladro.travelapp.hotel.domain.entity;

import com.kubisfladro.travelapp.hotel.domain.dto.CreateHotelDto;
import com.kubisfladro.travelapp.hotel.domain.enumerate.Stars;
import com.sun.istack.NotNull;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity
@Table(name = "hotels")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Hotel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "hotel_id")
    Long id;

    @NotNull
    @Column(name = "city")
    String city;

    @NotNull
    @Column(name = "name")
    String name;

    @NotNull
    @Column(name = "address")
    String address;

    @NotNull
    @Column(name = "stars")
    Stars stars;

    @NotNull
    @Column(name = "image_url")
    String imageUrl;

}
