package com.kubisfladro.travelapp.hotel.domain.exception;

public class RoomReservationsExistException extends RuntimeException{
    public RoomReservationsExistException() {
    }

    public RoomReservationsExistException(String message) {
        super(message);
    }
}
