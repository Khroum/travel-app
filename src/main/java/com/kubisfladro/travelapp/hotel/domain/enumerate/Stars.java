package com.kubisfladro.travelapp.hotel.domain.enumerate;

public enum Stars {
    NONE,
    ONE,
    TWO,
    THREE,
    FOUR,
    FIVE
}
