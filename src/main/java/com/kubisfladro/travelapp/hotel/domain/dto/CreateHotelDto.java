package com.kubisfladro.travelapp.hotel.domain.dto;

import com.kubisfladro.travelapp.hotel.domain.enumerate.Stars;
import lombok.*;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateHotelDto {
    String city;
    String name;
    String address;
    Stars stars;
    String imageUrl;
}
