package com.kubisfladro.travelapp.hotel.domain.entity;

import com.kubisfladro.travelapp.hotel.domain.dto.CreateRoomDto;
import com.kubisfladro.travelapp.hotel.domain.enumerate.Standard;
import com.sun.istack.NotNull;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity
@Table(name = "hotel_rooms")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "room_id")
    Long roomId;

    @NotNull
    @Column(name = "room_number")
    Integer roomNumber;

    @NotNull
    @Column(name = "hotel_id")
    Long hotelId;

    @NotNull
    @Column(name = "standard")
    Standard standard;

    @NotNull
    @Column(name = "number_of_beds")
    Integer numberOfBeds;

    @NotNull
    @Column(name = "daily_cost")
    Double dailyCost;

    @NotNull
    @Column(name = "available")
    Boolean available;

    @NotNull
    @Column(name = "image_url")
    String imageUrl;

}
