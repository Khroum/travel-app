package com.kubisfladro.travelapp.hotel.domain.entity;

import com.sun.istack.NotNull;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "room_reservations")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RoomReservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "reservation_id")
    Long id;

    @NotNull
    @Column(name = "hotel_id")
    Long hotelId;

    @NotNull
    @Column(name = "order_id")
    Long orderId;

    @NotNull
    @Column(name = "room_number")
    Integer roomNumber;

    @NotNull
    @Column(name = "from_date", columnDefinition = "DATE")
    LocalDate from;

    @NotNull
    @Column(name = "to_date", columnDefinition = "DATE")
    LocalDate to;

    @NotNull
    @Column(name = "reservation_cost")
    Double reservationCost;

}
