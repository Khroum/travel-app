package com.kubisfladro.travelapp.hotel.domain.repository;

import com.kubisfladro.travelapp.hotel.domain.entity.Hotel;
import com.kubisfladro.travelapp.hotel.domain.enumerate.Stars;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface HotelRepository extends JpaRepository<Hotel,Long> {
    List<Hotel> findHotelsByStars(Stars stars);
    List<Hotel> findHotelsByCity(String city);
}
