package com.kubisfladro.travelapp.hotel.domain.repository;

import com.kubisfladro.travelapp.hotel.domain.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoomRepository extends JpaRepository<Room,Long> {
    List<Room> findRoomsByHotelId(Long id);
    void deleteRoomsByHotelId(Long id);
    Optional<Room> findRoomByRoomNumberAndHotelId(Integer roomNumber, Long hotelId);
}
