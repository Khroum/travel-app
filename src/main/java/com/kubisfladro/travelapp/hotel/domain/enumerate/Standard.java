package com.kubisfladro.travelapp.hotel.domain.enumerate;

public enum Standard {
    STANDARD,
    FAMILY,
    SUITE
}
