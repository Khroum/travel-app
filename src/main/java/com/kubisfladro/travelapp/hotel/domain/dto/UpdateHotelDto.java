package com.kubisfladro.travelapp.hotel.domain.dto;

import com.kubisfladro.travelapp.hotel.domain.enumerate.Stars;
import lombok.*;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UpdateHotelDto {
    Long hotelId;
    String city;
    String name;
    String address;
    Stars stars;
    String imageUrl;
}
