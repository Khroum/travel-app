package com.kubisfladro.travelapp.hotel.domain.dto;

import com.kubisfladro.travelapp.hotel.domain.enumerate.Standard;
import lombok.*;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateRoomDto {
    Integer roomNumber;
    Long hotelId;
    Standard standard;
    Integer numberOfBeds;
    Double dailyCost;
    Boolean available;
    String imageUrl;
}
