package com.kubisfladro.travelapp.hotel.domain.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateRoomReservationDto {
    Long hotelId;
    Long orderId;
    Integer roomNumber;
    LocalDate from;
    LocalDate to;
    Double reservationCost;
}
