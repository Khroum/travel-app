package com.kubisfladro.travelapp.transport.service;

import com.kubisfladro.travelapp.transport.domain.dto.DestinationDto;
import com.kubisfladro.travelapp.transport.domain.dto.UpdateTransportDto;
import com.kubisfladro.travelapp.transport.domain.entity.Transport;
import com.kubisfladro.travelapp.transport.domain.repository.TransportRepository;
import com.kubisfladro.travelapp.transport.domain.dto.CreateTransportDto;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class TransportServiceImpl implements TransportService{
    private TransportRepository transportRepository;

    @Autowired
    public TransportServiceImpl(TransportRepository transportRepository) {
        this.transportRepository = transportRepository;
    }

    @Override
    public void addTransport(CreateTransportDto createTransportDto) {
        transportRepository.save(fromDto(createTransportDto));
    }

    @Override
    public void deleteTransportsByContractor(String contractor) {
        transportRepository.deleteTransportsByContractor(contractor);
    }

    @Override
    public void deleteTransportById(Long transportId) {
        transportRepository.deleteById(transportId);
    }

    @Override
    public void changeTransportDetails(UpdateTransportDto updateTransportDto) {
        transportRepository.findById(updateTransportDto.getTransportId())
                .ifPresent(transport -> {
                    transport.setType(updateTransportDto.getType());
                    transport.setTransportClass(updateTransportDto.getTransportClass());
                    transport.setContractor(updateTransportDto.getContractor());
                    transport.setFromDestination(updateTransportDto.getFromDestination());
                    transport.setToDestination(updateTransportDto.getToDestination());
                    transport.setNumberOfSeats(updateTransportDto.getNumberOfSeats());
                    transport.setTicketCost(updateTransportDto.getTicketCost());
                    transport.setWebsiteUrl(updateTransportDto.getWebsiteUrl());
                    transport.setLogoUrl(updateTransportDto.getLogoUrl());
                    transportRepository.save(transport);
                });
    }

    @Override
    public List<Transport> getTransportsByDestination(DestinationDto destinationDto) {
        return transportRepository.findTransportsByFromDestinationAndToDestination(destinationDto.getFrom(), destinationDto.getTo());
    }


    @Override
    public List<Transport> getAllTransports() {
        return transportRepository.findAll();
    }

    @Override
    public Transport getTransportById(Long id) {
        return transportRepository.findById(id).get();
    }

    private Transport fromDto(CreateTransportDto createTransportDto){
        return Transport
                .builder()
                .type(createTransportDto.getType())
                .transportClass(createTransportDto.getTransportClass())
                .contractor(createTransportDto.getContractor())
                .fromDestination(createTransportDto.getFromDestination())
                .toDestination(createTransportDto.getToDestination())
                .numberOfSeats(createTransportDto.getNumberOfSeats())
                .ticketCost(createTransportDto.getTicketCost())
                .websiteUrl(createTransportDto.getWebsiteUrl())
                .logoUrl(createTransportDto.getLogoUrl())
                .build();
    }
}
