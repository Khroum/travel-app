package com.kubisfladro.travelapp.transport.service;

import com.kubisfladro.travelapp.transport.domain.dto.DestinationDto;
import com.kubisfladro.travelapp.transport.domain.dto.UpdateTransportDto;
import com.kubisfladro.travelapp.transport.domain.entity.Transport;
import com.kubisfladro.travelapp.transport.domain.dto.CreateTransportDto;

import java.util.List;

public interface TransportService {
    void addTransport(CreateTransportDto createTransportDto);
    void deleteTransportsByContractor(String contractor);
    void deleteTransportById(Long transportId);
    void changeTransportDetails(UpdateTransportDto updateTransportDto);
    List<Transport> getTransportsByDestination(DestinationDto destinationDto);
    List<Transport> getAllTransports();
    Transport getTransportById(Long id);
}
