package com.kubisfladro.travelapp.transport;

import com.kubisfladro.travelapp.transport.domain.dto.DestinationDto;
import com.kubisfladro.travelapp.transport.domain.dto.UpdateTransportDto;
import com.kubisfladro.travelapp.transport.domain.entity.Transport;
import com.kubisfladro.travelapp.transport.domain.dto.CreateTransportDto;
import com.kubisfladro.travelapp.transport.domain.enumerate.TransportType;
import com.kubisfladro.travelapp.transport.service.TransportService;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/transport")
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class TransportController {
    TransportService transportService;

    @Autowired
    public TransportController(TransportService transportService) {
        this.transportService = transportService;
    }

    @CrossOrigin
    @PostMapping("/create-transport")
    public void createTransport(@RequestBody CreateTransportDto createTransportDto){
        transportService.addTransport(createTransportDto);
    }

    @CrossOrigin
    @GetMapping("/get-all-transports")
    public ResponseEntity<List<Transport>> getAllTransports(){
        return new ResponseEntity<>(transportService.getAllTransports(), HttpStatus.OK);
    }

    @CrossOrigin
    @DeleteMapping("/delete-transports-by-contractor/{contractor}")
    void deleteTransportsByContractor(@PathVariable String contractor){
        transportService.deleteTransportsByContractor(contractor);
    }

    @CrossOrigin
    @DeleteMapping("/delete-transport-by-id/{id}")
    void deleteTransportById(@PathVariable Long id){
        transportService.deleteTransportById(id);
    }

    @CrossOrigin
    @PostMapping("/change-transport-details")
    void changeTransportDetails(@RequestBody UpdateTransportDto updateTransportDto){
        transportService.changeTransportDetails(updateTransportDto);
    }

    @CrossOrigin
    @GetMapping("/get-transports-by-destination/{from}/{to}")
    ResponseEntity<List<Transport>> getTransportsByDestination(@PathVariable String from, @PathVariable String to){
        return ResponseEntity.ok(transportService.getTransportsByDestination(new DestinationDto(from,to)));
    }

    @CrossOrigin
    @GetMapping("/get-transport-by-id/{id}")
    ResponseEntity<Transport> getTransportById(@PathVariable Long id){
        return ResponseEntity.ok(transportService.getTransportById(id));
    }

}
