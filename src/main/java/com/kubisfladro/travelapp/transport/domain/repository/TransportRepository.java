package com.kubisfladro.travelapp.transport.domain.repository;

import com.kubisfladro.travelapp.transport.domain.entity.Transport;
import com.kubisfladro.travelapp.transport.domain.enumerate.TransportType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransportRepository extends JpaRepository<Transport,Long> {
    List<Transport> findTransportsByFromDestinationAndToDestination(String from, String to);
    void deleteTransportsByContractor(String contractor);
}
