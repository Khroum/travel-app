package com.kubisfladro.travelapp.transport.domain.enumerate;

public enum TransportType {
    AIRPLANE,
    BUS,
    TRAIN,
    METRO
}
