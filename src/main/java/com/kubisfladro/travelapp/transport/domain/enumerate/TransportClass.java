package com.kubisfladro.travelapp.transport.domain.enumerate;

public enum TransportClass {
    ECONOMY,
    STANDARD,
    BUSINESS
}
