package com.kubisfladro.travelapp.transport.domain.dto;

import com.kubisfladro.travelapp.transport.domain.enumerate.TransportClass;
import com.kubisfladro.travelapp.transport.domain.enumerate.TransportType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UpdateTransportDto {
    Long transportId;
    TransportType type;
    TransportClass transportClass;
    String contractor;
    String fromDestination;
    String toDestination;
    Integer numberOfSeats;
    Double ticketCost;
    String websiteUrl;
    String logoUrl;
}
