package com.kubisfladro.travelapp.transport.domain.entity;

import com.kubisfladro.travelapp.transport.domain.enumerate.TransportClass;
import com.kubisfladro.travelapp.transport.domain.enumerate.TransportType;
import com.sun.istack.NotNull;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity
@Table(name = "transport")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Transport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transport_id")
    Long id;

    @NotNull
    @Column(name = "transport_type")
    TransportType type;

    @NotNull
    @Column(name = "transport_class")
    TransportClass transportClass;

    @NotNull
    @Column(name = "contractor")
    String contractor;

    @NotNull
    @Column(name = "from_destination")
    String fromDestination;

    @NotNull
    @Column(name = "to_destination")
    String toDestination;

    @NotNull
    @Column(name = "number_of_seats")
    Integer numberOfSeats;

    @NotNull
    @Column(name = "ticket_cost")
    Double ticketCost;

    @NotNull
    @Column(name = "website_url")
    String websiteUrl;

    @NotNull
    @Column(name = "logo_url")
    String logoUrl;

}
