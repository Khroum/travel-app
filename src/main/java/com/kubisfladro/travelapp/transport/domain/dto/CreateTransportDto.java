package com.kubisfladro.travelapp.transport.domain.dto;

import com.kubisfladro.travelapp.transport.domain.enumerate.TransportClass;
import com.kubisfladro.travelapp.transport.domain.enumerate.TransportType;
import lombok.*;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateTransportDto {
    TransportType type;
    TransportClass transportClass;
    String contractor;
    String fromDestination;
    String toDestination;
    Integer numberOfSeats;
    Double ticketCost;
    String websiteUrl;
    String logoUrl;
}
