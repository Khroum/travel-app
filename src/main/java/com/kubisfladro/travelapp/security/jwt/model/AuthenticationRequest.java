package com.kubisfladro.travelapp.security.jwt.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class AuthenticationRequest {
    String username;
    String password;
}
